const connection = require('./connect');
const BPromise = require('bluebird');

class Query {

  migrate(sql, opts) {
    return new BPromise((resolve, reject) => {
      return connection().then(conn => {
        return conn.query(sql, null, (error, results, fields) => {
          if (error)
            return reject(error);
          return resolve(results);
        });
      });
    });
  }

}

module.exports = Query;