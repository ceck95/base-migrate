const config = require(`${process.cwd()}/config/db.json`);
const mysql = require('mysql');
const BPromise = require('bluebird');

class Connect {

  connect() {
    return new BPromise((resolve, reject) => {
      const mysqlCfg = config.mysql,
        con = mysql.createConnection({
          host: mysqlCfg.host,
          user: mysqlCfg.user,
          password: mysqlCfg.password
        });

      return con.connect((err) => {
        if (err) throw err;
        const database = config.mysql.database;
        con.query(`CREATE SCHEMA IF NOT EXISTS ${database} DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;`, (err, result) => {
          const connection = mysql.createConnection(config.mysql);
          return resolve(connection);
        });
      });
    });
  }

  run() {
    return new BPromise((resolve, reject) => {
      if (config.mysql) {
        return this.connect().then(connection => {
          connection.connect((err) => {
            if (err) {
              console.error('error connecting: ' + err.stack);
              return reject(err);
            }
          });
          return resolve(connection);
        }).catch(err => {
          console.error('error create init: ' + err.stack);
        });
      } else {
        console.log('Config mysql not null');
      }
    });

  }
}

const ConnectClass = new Connect();
ConnectClass.run();
module.exports = ConnectClass.connect;