const dirUpdate = 'updates';
const dirInit = 'init';
const BPromise = require('bluebird');
const fs = BPromise.promisifyAll(require('fs'));
const path = require('path');
const fileHistory = path.join(process.cwd(), 'history.txt');
const config = require(`${process.cwd()}/config/db.json`);
const exec = require('child_process').exec;


class Migrate {

  updates(opts) {
    const fullPathUpdate = path.join(process.cwd(), dirUpdate);
    return BPromise.resolve(this.commonGetFiles(fullPathUpdate, 'updates'));
  }

  commonGetFiles(path, position, opts) {
    const func = () => {
      return fs.readdirAsync(path).then(files => {
        if (opts)
          return this.migrateCommon(files, position, path, opts);
        return this.migrateCommon(files, position, path);
      });
    };
    if (!fs.existsSync(path)) {
      return fs.mkdir(path, (err) => {
        return BPromise.resolve(func());
      });
    }
    return BPromise.resolve(func());
  }

  init() {
    const fullPathInt = path.join(process.cwd(), dirInit);
    return BPromise.resolve(this.commonGetFiles(fullPathInt, 'init'));
  }

  migrateCommon(files, position, path, opts) {

    return new BPromise((resolve, reject) => {
      const Query = require('./query'),
        query = new Query(),
        history = fs.createWriteStream(fileHistory, {
          flags: 'a'
        });

      return fs.readFile(fileHistory, 'utf8', (err, data) => {

        if (err) {
          return console.log(err);
        }
        let dataHistory = data.split('\r\n');
        dataHistory = dataHistory.filter(e => {
          if (e === '')
            return false;
          return true;
        });

        files = files.filter((e) => {
          let checkRemoveItem = false;
          dataHistory.every(a => {
            if (e === a) {
              checkRemoveItem = true;
              return false;
            }
            return true;
          });
          if (checkRemoveItem) {
            return false;
          }
          return true;
        });
        if (files.length === 0) {
          console.log(`Database of ${position}  was latest`);
          return resolve({
            status: 'success'
          });
        }
        console.log(`Count need migrate: ${files.length}`);
        console.log(`List file name:`);
        files.forEach((e, i) => {
          console.log(`STT ${i+1}: ${e}`);
        });
        const forEachMigrate = (index) => {

          if (index === files.length) {
            return resolve({
              status: 'success'
            });
          }
          let e = files[index];
          console.log('------------------------------------------------');
          console.log(`Migrating stt ${index+1} with file name is ${e}`);
          fs.readFile(`${path}/${e}`, 'utf8', (err, textSql) => {
            if (config.showLogQuery) {
              console.log(`SQL: ${textSql}`);
            }
            if (opts && opts.noMigrate) {
              console.log(`Migrated file name ${e}`);
              console.log('------------------------------------------------');
              history.write(`${e}\r\n`);
              return forEachMigrate(++index);
            } else {
              return query.migrate(textSql).then(results => {
                console.log(`Migrated file name ${e}`);
                console.log('------------------------------------------------');
                history.write(`${e}\r\n`);
                return forEachMigrate(++index);
              }).catch(err => {
                return reject(err);
              });
            }

          });

        };
        console.log('Migrating ...........');
        forEachMigrate(0);
      });

    });

  }

  createFile(name) {
    const fileName = `${(new Date()).getTime()}-${name || 'default'}.sql`,
      file = `${process.cwd()}/${dirUpdate}/${fileName}`;
    console.log(`Create file successfully: ${fileName}`);
    return BPromise.resolve(fs.openSync(file, 'w'));
  }

  sync() {
    return new BPromise((resolve, reject) => {
      const Query = require('./query'),
        query = new Query();
      return query.migrate(`DROP SCHEMA ${config.mysql.database};CREATE SCHEMA ${config.mysql.database};USE ${config.mysql.database};`).then(() => {
        return fs.readFile(path.join(process.cwd(), 'dump', 'database.sql'), 'utf8', (err, data) => {
          console.log('Syncing, please wait in seconds ...');
          return query.migrate(data).then(result => {
            console.log('Sync database sucessfully');
            return resolve(result);
          }).catch(err => {
            return reject(err);
          });
        });
      }).catch(err => {
        return reject(err);
      });
    });
  }

  backup() {
    return new BPromise((resolve, reject) => {
      const mysqlDump = require('mysqldump'),
        configMysql = config.mysql,
        fileBackup = `${process.cwd()}/dump/database.sql`;
      return fs.unlink(fileBackup, (err) => {
        if (err)
          return reject(err);

        const command = `mysqldump -u ${config.mysql.user} -h ${config.mysql.host} -p${config.mysql.password} ${config.mysql.database}`,
          file = fs.createWriteStream(fileBackup, {
            flags: 'a'
          });

        return exec(command, {
          maxBuffer: 50024 * 500
        }, function (error, stdout, stderr) {
          if (error !== null)
            return reject(err);
          console.log('Backup database successfully');
          file.write(stdout);
          return resolve({
            success: true
          });
        });
        // return mysqlDump({
        //   host: configMysql.host,
        //   user: configMysql.user,
        //   password: configMysql.password,
        //   database: configMysql.database,
        //   dest: fileBackup
        // }, function(err) {
        //   if (err)
        //     return reject(err);
        //   console.log('Backup database successfully');
        //   return resolve({
        //     backup: 'successfully'
        //   })
        // });
      });
    });
  }

  makeHistory(dir) {
    const handleMake = (dirCurrent) => {
      return BPromise.resolve(this.commonGetFiles(path.join(process.cwd(), dirCurrent), dirCurrent, {
        noMigrate: true
      }));
    };
    switch (dir) {
      case 'updates':
        return handleMake('updates');
      case 'init':
        return handleMake('updates');
      default:
        return BPromise.reject('Not exits directory');
    }
  }

}

module.exports = Migrate;