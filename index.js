const MigrateBase = require('./base/migrate');

class Migrate extends MigrateBase {

  run() {

    switch (process.argv[2]) {
      case '--help':
        console.log('--help: this is guide to you use migrate');
        console.log('--init: run sql init');
        console.log('--updates: run sql update');
        console.log('--create name=?: create file update');
        console.log('--sync: sync data');
        console.log('--backup: backup database');
        console.log('--makeHistory dir=[includes:updates,init]: make history foreach directory');
        process.exit();
        break;
      case '--init':
        this.init().then(data => {
          process.exit()
        }).catch(err => {
          console.error(err);
          process.exit();
        });
        break;
      case '--updates':
        this.updates().then(data => {
          process.exit();
        }).catch(err => {
          console.error(err);
          process.exit();
        });
        break;
      case '--sync':
        this.sync().then(() => {
          process.exit();
        }).catch(err => {
          console.error(err);
          process.exit();
        });
        break;
      case '--backup':
        this.backup().then(() => {
          process.exit();
        }).catch(err => {
          console.error(err);
          process.exit();
        });
        break;
      case '--create':
        if (/name=/g.test(process.argv[3])) {
          let name = process.argv[3].replace('name=', '');
          if (name !== '') {
            this.createFile(name);
          } else {
            console.error('file name not null , in create file update');
          }
        } else {
          console.error('Syntax error , in create file update');
        }
        process.exit();
        break;

      case '--makeHistory':
        if (/dir=/g.test(process.argv[3])) {
          let name = process.argv[3].replace('dir=', '');
          if (name !== '') {
            return this.makeHistory(name).then(() => {
              console.log('Make history successfully');
              process.exit();
            }).catch(err => {
              console.error('Make history has error', err);
              process.exit();
            });
          } else {
            console.error('dir name not null , in make history');
            process.exit();
          }
        } else {
          console.error('Syntax error , in make history');
          process.exit();
        }
        break;
      default:
        console.log('Please add argument --help for command line');
        process.exit();
        break;
    }

  }

}

module.exports = Migrate;